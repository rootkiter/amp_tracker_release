#!/bin/python
###############################################
# File Name : install_amp.py
#    Author : rootkiter
#    E-mail : rootkiter@rootkiter.com
#   Created : 2019-07-23 03:10:49 PDT
###############################################

import json
import sys,os,socket

ampidMap = [
    # qiyun shanghai C
    "101.198.176.15",
    "101.198.176.83",
    "101.198.176.74",
    "101.198.176.134",
    "101.198.176.228",
    "101.198.176.243",
    "101.198.176.50",
    "101.198.176.66",

    # qiyun beijing A
    "123.59.120.223",
    "123.59.120.159",
    "123.59.120.208",
    "123.59.120.186",
    "123.59.120.113",
    "123.59.120.39",
    "123.59.120.130",
    "123.59.120.234",

]

def copyFile ( release_dir, absinpath, outdir, overwrite=True ):
    infilepath = os.path.join(release_dir, absinpath)
    # check outdir
    dirtest="test -d %s || mkdir -p %s" % (outdir, outdir)
    print(dirtest)
    os.system(dirtest)
    if overwrite:
        cpcmd = "test -f %s && cp %s %s" % (
            infilepath, infilepath, outdir
        )
        print (cpcmd)
        os.system(cpcmd)
    else:
        filename = os.path.basename(infilepath)
        outfilepath= os.path.join(outdir, filename)
        cpcmd = "test -f %s || (test -f %s && cp %s %s)" % (
            outfilepath,
            infilepath,
            infilepath,
            outfilepath
        )
        print(cpcmd)
        os.system(cpcmd)

def moveFile ( srcfile, dstDir, overwrite = True ):
    dirtest="test -d %s || mkdir -p %s" % (dstDir, dstDir)
    print(dirtest)
    os.system(dirtest)

    if overwrite:
        mvcmd = "test -f %s && test -d %s && mv %s %s" % (
            srcfile, dstDir, srcfile, dstDir
        )
        print (mvcmd)
        os.system(mvcmd)

def get_http(host,url):
    import httplib
    conn = httplib.HTTPConnection(host)
    conn.request(method="GET",url=url) 
    response = conn.getresponse()
    res= response.read()
    return res.strip()


def getLip(ifacename):
    import fcntl,struct
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    hostip  = socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifacename[:15].encode())
    )[20:24])
    return hostip

def get_ifacename():
    routcmd = "cat /proc/net/route"
    routresult = os.popen(routcmd).read().split('\n')
    interface = []
    hexip    = []
    buf = {}
    for i in routresult:
        items = i.split()
        if(len(items) != 11):
            continue
        if(items[1] == '00000000'):
            if(items[0] not in interface):
                interface.append(items[0])
    if len(interface) > 0:
        return interface[0]

def configInit( outpath, v2 , pubip=None ):
    config = {}
    if pubip == None:
        config["pubip"] = get_http("ipecho.net","http://ipecho.net/plain")
    else:
        config['pubip'] = pubip
    config["facename"] = get_ifacename()
    config["hostip"]   = getLip(config["facename"])

    # black ip
    config["blackip"]  = []
    config["blackport"]= [2790]

    # remote control
    config["ctlIp"]    = "35.184.91.65"
    config["ctlPort"]  = 11290

    # scanflag
    config["scancheck"] = True

    config['v2'] = v2

    for i in range(0, len(ampidMap)):
        if ampidMap[i] == config["pubip"]:
            config["AmpID"] = i

    if "AmpID" not in config:
        config['AmpID'] = -1

    jsstr = json.dumps(config)

    outdir = outpath
    outdir = os.path.join(outdir,'config')
    if(not os.path.exists(outdir)):
        os.makedirs(outdir)

    with open(os.path.join(outdir,'config.json'),'w') as f:
        f.write(json.dumps(config)+"\n")
    print(jsstr)
    return True

def updateSh( releasepath , outpath ):
    module = "DIS_RELEASE_PATH=%s\n" % releasepath
    module+= "test -d $DIS_RELEASE_PATH || (echo \"NO DIR\" && exit)\n"
    module+= "cd $DIS_RELEASE_PATH && git config --global user.email \"rootkiter@rootkiter.com\" && git stash && git pull && python ./install_amp.py\n"

    with open(os.path.join(outpath,'update.sh'),'w') as f:
        f.write(module)

if __name__=='__main__':
    release_dir = os.path.dirname(os.path.abspath(__file__))
    if len(sys.argv) < 3:
        print("Eg: %s [v2] [pubip]", sys.argv[0])
    else:
        # sys.argv[1] is hostip
        # sys.argv[2] is v2
        configInit( release_dir , sys.argv[1], sys.argv[2])
        homepath = os.popen("cd ~ && pwd").read().strip()
        outdir   = os.path.join(homepath,"disguiser_go")

        copyFile(release_dir, "config/config.json"  , outdir, False )  # Don't overwrite
        copyFile(release_dir, "start_amp.sh"        , outdir        )  # update start.sh everytime
        copyFile(release_dir, "disguiser_agent"     , outdir        )  # update disguiser_agent everytime
        # updateSh (release_dir, outdir )    # update bash init

        chmod_x_cmd = "chmod a+x %s/start_amp.sh %s/disguiser_agent %s/update.sh" % (outdir, outdir, outdir)
        os.system(chmod_x_cmd)


        print ("$ echo \"*/5 *   * * *   root    "+outdir+"/start_amp.sh [username]\" > /etc/crontab")
        print ("$ echo \"*/5 *   * * *   "+outdir+"/start_amp.sh [username]\" > crontab -e")
        print ("$ ps -ef | grep dis | grep agent | grep -v flock | awk '{system(\"kill -9 \"$2)}' && ~/disguiser_go/start_amp.sh [username]")
